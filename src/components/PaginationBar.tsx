import Link from 'next/link'

interface PaginationBarProps {
  currentPage: number
  totalPages: number
}

export default function PaginationBar({
  currentPage,
  totalPages,
}: PaginationBarProps) {
  const maxPage = Math.min(totalPages, Math.max(currentPage + 4, 10))
  const minPage = Math.max(1, Math.min(currentPage - 5, maxPage - 9))

  const numberedPageItems: JSX.Element[] = []

  for (let page = minPage; page <= maxPage; page++) {
    numberedPageItems.push(
      <Link
        key={page}
        href={`?page=${page}`}
        className={`btn join-item ${
          currentPage === page && 'btn-active pointer-events-none'
        }`}
      >
        {page}
      </Link>,
    )
  }

  return (
    <>
      <section className='join hidden sm:block'>{numberedPageItems}</section>
      <section className='join sm:hidden'>
        {currentPage > 1 && (
          <Link href={`?page=${currentPage - 1}`} className='btn join-item'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              strokeWidth='2'
              strokeLinecap='round'
              strokeLinejoin='round'
            >
              <polygon points='19 20 9 12 19 4 19 20'></polygon>
              <line x1='5' y1='19' x2='5' y2='5'></line>
            </svg>
          </Link>
        )}
        <button className='btn join-item pointer-events-none'>
          Page {currentPage}
        </button>
        {currentPage < totalPages && (
          <Link href={`?page=${currentPage + 1}`} className='btn join-item'>
            <svg
              xmlns='http://www.w3.org/2000/svg'
              width='24'
              height='24'
              viewBox='0 0 24 24'
              fill='none'
              stroke='currentColor'
              strokeWidth='2'
              strokeLinecap='round'
              strokeLinejoin='round'
            >
              <polygon points='5 4 15 12 5 20 5 4'></polygon>
              <line x1='19' y1='5' x2='19' y2='19'></line>
            </svg>
          </Link>
        )}
      </section>
    </>
  )
}
