import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import SessionProvider from '@/app/SessionProvider'

import Navbar from '@/app/Navbar/Navbar'
import Footer from '@/app/Footer'
import './globals.css'

const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Nextazon',
  description: 'Mock ecommerce website project',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang='en'>
      <body className={`${inter.className} h-full`}>
        <SessionProvider>
          <Navbar />
          <main className='mx-auto min-w-[280px] max-w-7xl p-4'>
            {children}
          </main>
          <Footer />
        </SessionProvider>
      </body>
    </html>
  )
}
