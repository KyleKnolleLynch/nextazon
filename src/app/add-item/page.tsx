import { redirect } from 'next/navigation'
import { getServerSession } from 'next-auth'

import { prisma } from '@/lib/db/prisma'
import FormSubmitButton from '@/components/FormSubmitButton'
import { authOptions } from '../api/auth/[...nextauth]/route'

export const metadata = {
  title: 'Add Item',
}

async function addItem(formData: FormData) {
  'use server'

  const session = await getServerSession(authOptions)

  if (!session) redirect('/api/auth/signin?callbackUrl=/add-item')

  const name = formData.get('name')?.toString()
  const description = formData.get('description')?.toString()
  const imageUrl = formData.get('imageUrl')?.toString()
  const price = Number(formData.get('price') || 0)

  if (!name || !description || !imageUrl || !price) {
    throw Error('Missing required fields')
  }

  await prisma.product.create({
    data: { name, description, imageUrl, price },
  })

  redirect('/')
}

export default async function AddItemPage() {
  const session = await getServerSession(authOptions)

  if (!session) redirect('/api/auth/signin?callbackUrl=/add-item')

  return (
    <div>
      <h1 className='mb-3 text-2xl font-bold'>Add Product</h1>
      <form action={addItem}>
        <input
          type='text'
          className='input input-bordered mb-3 w-full'
          name='name'
          placeholder='Name'
          required
        />
        <textarea
          className='textarea textarea-bordered mb-3 w-full'
          name='description'
          placeholder='Description'
          required
        ></textarea>
        <input
          type='url'
          className='input input-bordered mb-3 w-full'
          name='imageUrl'
          placeholder='Image URL'
          required
        />{' '}
        <input
          type='number'
          className='input input-bordered mb-3 w-full'
          name='price'
          placeholder='Price'
          required
        />
        <FormSubmitButton className='btn-block'>Add Item</FormSubmitButton>
      </form>
    </div>
  )
}
