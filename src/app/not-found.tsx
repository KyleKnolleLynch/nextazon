import Link from 'next/link'

export default function NotFoundPage() {
  return (
    <section className='mt-6 min-h-screen text-center'>
      <h1 className='text-2xl sm:text-3xl'>Page not found.</h1>
      <Link href='/' className='btn btn-outline mt-6'>Go Home</Link>
    </section>
  )
}
