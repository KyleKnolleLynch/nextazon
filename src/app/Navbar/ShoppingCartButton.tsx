'use client'

import Link from 'next/link'

import { ShoppingCart } from '@/lib/db/cart'
import { formatPrice } from '@/lib/format'

interface ShoppingCartButtonProps {
  cart: ShoppingCart | null
}

export default function ShoppingCartButton({ cart }: ShoppingCartButtonProps) {
  function closeDropdown() {
    const elem = document.activeElement as HTMLElement
    if (elem) {
      elem.blur()
    }
  }

  return (
    <div className='dropdown dropdown-end'>
      <label tabIndex={0} className='btn btn-circle btn-ghost'>
        <div className='indicator'>
          <svg
            xmlns='http://www.w3.org/2000/svg'
            viewBox='0 0 24 24'
            fill='none'
            stroke='currentColor'
            strokeWidth='2'
            strokeLinecap='round'
            strokeLinejoin='round'
            className='h-5 w-5'
          >
            <circle cx='9' cy='21' r='1'></circle>
            <circle cx='20' cy='21' r='1'></circle>
            <path d='M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6'></path>
          </svg>
          <span className='badge indicator-item badge-secondary badge-sm'>
            {cart?.size || 0}
          </span>
        </div>
      </label>
      <div
        tabIndex={0}
        className='card dropdown-content card-compact z-30 mt-4 w-52 bg-base-100 shadow'
      >
        <div className='card-body'>
          <span className='text-lg font-bold'>{cart?.size || 0} Items</span>
          <span className='text-info'>
            Subtotal: {formatPrice(cart?.subtotal || 0)}
          </span>
          <div className='card-actions'>
            <Link
              href='/cart'
              className='btn btn-primary btn-block'
              onClick={closeDropdown}
            >
              View Cart
            </Link>
          </div>
        </div>
      </div>
    </div>
  )
}
