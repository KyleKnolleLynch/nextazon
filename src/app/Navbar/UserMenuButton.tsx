'use client'

import { Session } from 'next-auth'
import Image from 'next/image'
import { signIn, signOut } from 'next-auth/react'

import profilePlaceholder from '@/assets/profile_placeholder.png'

interface UserMenuButtonProps {
  session: Session | null
}

export default function UserMenuButton({ session }: UserMenuButtonProps) {
  const user = session?.user
  return (
    <div className='dropdown dropdown-end'>
      <label tabIndex={0} className='btn btn-circle btn-ghost'>
        {user ? (
          <Image
            src={user?.image || profilePlaceholder}
            alt='profile_avatar'
            width={40}
            height={40}
            className='w-10 rounded-full'
          />
        ) : (
          <svg
            xmlns='http://www.w3.org/2000/svg'
            width='24'
            height='24'
            viewBox='0 0 24 24'
            fill='none'
            stroke='currentColor'
            strokeWidth='2'
            strokeLinecap='round'
            strokeLinejoin='round'
            className='h-[24px] w-[24px]'
          >
            <circle cx='12' cy='12' r='1'></circle>
            <circle cx='19' cy='12' r='1'></circle>
            <circle cx='5' cy='12' r='1'></circle>
          </svg>
        )}
      </label>
      <ul
        tabIndex={0}
        className='menu dropdown-content rounded-box menu-sm z-30 mt-3 w-52 bg-base-100 p-2 shadow'
      >
        <li>
          {user ? (
            <button onClick={() => signOut({ callbackUrl: '/' })}>
              Sign Out
            </button>
          ) : (
            <button onClick={() => signIn()}>Sign In</button>
          )}
        </li>
      </ul>
    </div>
  )
}
