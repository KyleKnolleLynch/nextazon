import Image from 'next/image'
import Link from 'next/link'
import { redirect } from 'next/navigation'
import { getServerSession } from 'next-auth'

import logo from '@/app/favicon.ico'
import ShoppingCartButton from './ShoppingCartButton'
import { getCart } from '@/lib/db/cart'
import { authOptions } from '../api/auth/[...nextauth]/route'
import UserMenuButton from './UserMenuButton'

async function searchProducts(formData: FormData) {
  'use server'

  const searchQuery = formData.get('searchQuery')?.toString()

  if (searchQuery) {
    redirect(`/search?query=${searchQuery}`)
  }
}

export default async function Navbar() {
  const session = await getServerSession(authOptions)
  const cart = await getCart()

  return (
    <header className='bg-base-100'>
      <nav
        className='navbar mx-auto max-w-7xl flex-col gap-2 sm:flex-row'
        aria-label='Primary'
      >
        <div className='flex-1'>
          <Link href='/' className='btn btn-ghost text-xl normal-case'>
            <Image
              src={logo}
              alt='Nextazon logo'
              width={35}
              height={35}
              priority
              className='mr-1'
            />
            Nextazon
          </Link>
        </div>
        <div className='flex-none gap-2'>
          <form action={searchProducts}>
            <div className='form-control'>
              <input
                type='text'
                name='searchQuery'
                placeholder='Search'
                className='input input-bordered w-full min-w-[100px]'
              />
            </div>
          </form>
          <ShoppingCartButton cart={cart} />
          <UserMenuButton session={session} />
        </div>
      </nav>
    </header>
  )
}
