'use client'

export default function ErrorPage() {
  return (
    <section className='mt-4 text-center'>
      <h1 className='mb-4 text-2xl font-semibold text-red-500 sm:text-3xl'>
        Oops, something went wrong.
      </h1>
      <button
        onClick={() => window.location.reload()}
        className='btn btn-outline'
      >
        Refresh Page
      </button>
    </section>
  )
}
