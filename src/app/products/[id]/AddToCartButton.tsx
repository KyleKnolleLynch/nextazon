'use client'

import { spawn } from 'child_process'
import { useState, useTransition } from 'react'

interface AddToCartButtonProps {
  productId: string
  incrementProductQuantity: (productId: string) => Promise<void>
}

export default function AddToCartButton({
  productId,
  incrementProductQuantity,
}: AddToCartButtonProps) {
  const [isPending, startTransition] = useTransition()
  const [success, setSuccess] = useState(false)

  return (
    <div className='flex items-center gap-2'>
      <button
        className='btn btn-primary'
        onClick={() => {
          setSuccess(false)
          startTransition(async () => {
            await incrementProductQuantity(productId)
            setSuccess(true)
          })
        }}
      >
        Add to Cart
        <svg
          xmlns='http://www.w3.org/2000/svg'
          viewBox='0 0 24 24'
          fill='none'
          stroke='currentColor'
          strokeWidth='2'
          strokeLinecap='round'
          strokeLinejoin='round'
          className='h-5 w-5'
        >
          <circle cx='9' cy='21' r='1'></circle>
          <circle cx='20' cy='21' r='1'></circle>
          <path d='M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6'></path>
        </svg>
      </button>
      {isPending && (
        <span className='loading loading-spinner loading-md'></span>
      )}
      {!isPending && success && (
        <span className='text-success'>Added to Cart</span>
      )}
    </div>
  )
}
