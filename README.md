# Nextazon E-Commerce Website

- Next.js

- Typescript

- Next Auth

- Prisma

- MongoDB

- TailwindCSS

- Daisy UI

- Zod (for typing environment variables)

### 🔗 [Check out the live project](https://nextazon.vercel.app)

This app uses as many server components as possible, only using client components when necessary, mainly for interactivity, and nesting them like "islands" within the server components.

This project uses Next.js server actions for the creating, updating, and deleting of products from the shopping cart, both in client components and server components. Shopping cart data is stored in the database, so a user can start an anonymous cart or sign in with Google authentication using Next Auth, and if they have items in both of those carts, the carts will merge seamlessly.

Credit and big thanks to [Coding in Flow](https://www.youtube.com/@codinginflow) for the inspiration and tutorial for this project. I re-created as much of this project as I could on my own but went back and followed the tutorial for the tricky bits that were too challenging for me to figure out myself.
