const config = {
  singleQuote: true,
  jsxSingleQuote: true,
  semi: false,
  arrowParens: 'avoid',
  plugins: ['prettier-plugin-tailwindcss'],
}

module.exports = config
