import type { Config } from 'tailwindcss'

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {},
  },
  plugins: [require('daisyui')],
  daisyui: {
    themes: [
      {
        mytheme: {
          primary: '#ea9567',
          secondary: '#f95640',
          accent: '#55dbbc',
          neutral: '#1e2025',
          'base-100': '#35383b',
          info: '#9ec0f0',
          success: '#5cd6c4',
          warning: '#dc8418',
          error: '#e72354',
          body: {
            'background-color': '#272935'
          },
        },
      },
    ],
  },
}
export default config
